from views.finance import GetOHLCResource, GetSummaryResource


def add_resources(api):

    api.add_resource(GetOHLCResource, '/getohlc')
    api.add_resource(GetSummaryResource, '/summary')