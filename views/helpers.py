import json
from decimal import Decimal

import numpy as np
import pandas as pd
from yfinance import Ticker, download


def get_ohlc(args):
    """Получение котировок OHLC

    Args:
        args (dict): десериализованные аргументы из запроса(формат JSON).

    Returns:
        data: список котировок по выбранным акциям
    """
    data = download(args.get('tickers'), start=args.get('start'), end=args.get('end'))

    pd.concat([data]).to_json("tickersconcat.json")

    data_json = pd.read_json("tickersconcat.json")

    return data_json


def get_summary(date=None):
    """[summary]
    """
    data_json = pd.read_json("tickersconcat.json").to_json()

    data = json.loads(data_json)
    key_list = tuple([item for item in data.keys() if 'Adj Close' in item])
    instruments_keys = tuple(replace_str(item)[1] for item in key_list)
    result_dict = dict()
    for instrument in instruments_keys:
        for item in key_list:
            if instrument in item:
                result_dict[instrument] = 100 - ((data[item][list(data[item].keys())[0]] / data[item][list(data[item].keys())[-1]]) * 100)

    return result_dict


def replace_str(str_):
    return str_.replace('(', '').replace(')', '').replace("'", '').replace(' ', '').split(',')
