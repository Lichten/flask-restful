import json
from http import HTTPStatus

from flask_restful import Resource
from webargs.flaskparser import use_args

from serializers.get_ohlc import GetOHLCSerializer
from views.helpers import get_ohlc, get_summary


class GetOHLCResource(Resource):

    @use_args(GetOHLCSerializer)
    def get(self, args):

        data = get_ohlc(args).to_json()

        return json.loads(data), HTTPStatus.OK


class GetSummaryResource(Resource):

    def get(self):
        return get_summary(), HTTPStatus.OK
