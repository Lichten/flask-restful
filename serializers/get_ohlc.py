import re

from marshmallow import Schema, fields, validate


class GetOHLCSerializer(Schema):
    tickers = fields.List(fields.Str())
    start = fields.Str(validate=validate.Regexp(r"[\d]{4}-[\d]{1,2}-[\d]{1,2}"))
    end = fields.Str(validate=validate.Regexp(r"[\d]{4}-[\d]{1,2}-[\d]{2}"))
