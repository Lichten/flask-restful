# flask restful

**Установка всех модулей в виртуальном окружении**

`pipenv install`


**Для этого необходимо установить pipenv**

`pip install pipenv`

**Файл конфигурации для запросов к апи через postman необходимо импортировать из файла** 

`flask_restful_api.postman_collection.json`

**Запуск осуществляется следующим образом:**

`pipenv shell`
`python app.py`